console.info('Pocket Sauron Viewer: Application Initialized. This comes form a typescript file.');

import '../node_modules/leaflet/dist/leaflet.css';
import * as Leaflet from 'Leaflet';

const map = Leaflet.map('map').setView([51.505, -0.09], 13);

Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

const getHistoryPoints = () => {
    const request = new XMLHttpRequest();
    request.open("GET", "http://77.55.222.49:3000/points/", true);

    request.onreadystatechange = function () {
        console.log(request);
        console.log(request.responseText);

        if (request && request.responseText) {
            let responsePoints = [];
            try {
                responsePoints = JSON.parse(request.responseText);
            } catch (e) {
                console.error(e);
            }

            const polyLineNodes = [];
            for (const point of responsePoints) {
                polyLineNodes.push([point.latitude, point.longitude]);
            }

            console.log(polyLineNodes);

            const polyline = Leaflet.polyline(polyLineNodes).addTo(map);
            map.fitBounds(polyline.getBounds());
        }
    };

    request.send("");
};

getHistoryPoints();

const trackerMarkers = {};

let pollNumber = 0;
const updatePosition = () => {
    const request = new XMLHttpRequest();
    request.open("GET", "http://77.55.222.49:3000/trackers/", true);
    request.onload = () => {
        pollNumber += 1;
        // console.log(request.responseText);
        if (request.responseText) {
            const responseTrackers = JSON.parse(request.responseText);
            console.log(pollNumber, responseTrackers);

            for (const tracker of responseTrackers) {

                let showThisTracker = false;
                if (tracker && tracker.latitude && tracker.longitude && tracker.serverTime) {
                    const timeDiff = Date.now() - tracker.serverTime;
                    console.log(timeDiff);
                    if (timeDiff < 60000) {
                        showThisTracker = true;
                    }
                }

                if (showThisTracker) {

                    if (trackerMarkers[tracker.trackerName]) {
                        trackerMarkers[tracker.trackerName].setLatLng([tracker.latitude, tracker.longitude]);
                    } else {
                        const tooltip = Leaflet.tooltip({
                            permanent: true
                        }).setContent(tracker.trackerName);

                        trackerMarkers[tracker.trackerName] = Leaflet.marker([tracker.latitude, tracker.longitude])
                            .addTo(map)
                            .bindTooltip(tooltip)
                            .openTooltip();
                    }

                }

            }

            console.log(trackerMarkers);
        }

    };

    request.send("");
};

// Leaflet.marker([48.1539311, 11.478171]).addTo(map).bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
//     .openPopup();

setInterval(() => {
    updatePosition();
}, 1000);


