import React, {Component} from 'react';
import {Button, StyleSheet, Text, TextInput, View} from 'react-native';

export default function App() {
    return (
        <View style={styles.container}>
            <Text>
                O_O --- POCKET SAURON --- O_O
            </Text>
            <Sauron></Sauron>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

class Sauron extends Component {
    state: any;

    constructor(props) {
        super(props);
        this.state = {
            serverAddress: "77.55.222.49:3000",
            trackerName: '',
            connected: false,
            pollInterval: null,
            currentTrackerId: null
        };
        console.log('constructor!');
    }

    connectToServer() {
        this.initializeTrackerData();
        this.initGeolocationSensorPolling();
        console.log('CONNECTING TO SERVER');
        fetch(`http://${this.state.serverAddress}/points`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
            //Request Type
        }).then(
            () => {
                this.setState({
                    connected: true
                });
                console.log('succ');
            }
        ).catch(
            () => {
                this.setState({
                    connected: false
                });
                console.log('err');
            }
        ).finally(
            () => {
                console.log('fin');
            }
        );
    }

    disconnectFromServer() {
        this.setState({connected: false});
        this.stopGeolocationSensorPolling();
    }


    savePointToServer(point) {
        console.log('saving point to server: ');
        if (this.state.trackerName) {
            fetch(`http://${this.state.serverAddress}/points`, {
                method: "POST",
                body: JSON.stringify({
                    trackerName: this.state.trackerName,
                    sensorTime: point.timestamp,
                    serverTime: Date.now(),
                    latitude: point.latitude,
                    longitude: point.longitude,
                    altitude: point.altitude,
                    speed: point.speed,
                    heading: point.heading
                }),
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then((jsonStoreResp) => {
                const formattedJSRPromise = jsonStoreResp.text();
                return formattedJSRPromise;
            }).then((formattedJSONStoreResp) => {
                console.log('saved point!')
            }).catch((error) => {
                console.log('JSON STORE POST ERROR: ', error);
            });
        }
    }

    initializeTrackerData() {
        console.log('get trackers:');
        if (this.state.trackerName) {
            fetch(`http://${this.state.serverAddress}/trackers/`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then((jsonStoreResp) => {
                const formattedJSRPromise = jsonStoreResp.text();
                return formattedJSRPromise;
            }).then((formattedJSONStoreResp) => {
                const trackers = JSON.parse(formattedJSONStoreResp);
                console.log('GET TRACKERS', trackers);
                let currentTrackerId = null;
                for (const tracker of trackers) {
                    if (tracker.trackerName === this.state.trackerName) {
                        currentTrackerId = tracker.id;
                        console.log('FOUND ID:', currentTrackerId);
                        this.setState({
                            currentTrackerId
                        })
                    }
                }
                if (!currentTrackerId) {
                    this.postNewTrackerData();
                }

            }).catch((error) => {
                console.log('trackers get error: ', error);
            });
        }

    }

    postNewTrackerData() {
        console.log('postNewTrackerData');
        fetch(`http://${this.state.serverAddress}/trackers`, {
            method: "POST",
            body: JSON.stringify({
                trackerName: this.state.trackerName
            }),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then((jsonStoreResp) => {
            const formattedJSRPromise = jsonStoreResp.text();
            return formattedJSRPromise;
        }).then((formattedJSONStoreResp) => {
            console.log('trackers post resp: ', formattedJSONStoreResp);
            const currentTrackerId = JSON.parse(formattedJSONStoreResp).id;

            console.log('NEW ID:', currentTrackerId);
            if (currentTrackerId){
                this.setState({
                    currentTrackerId
                })
            }

        }).catch((error) => {
            console.log('trackers post error: ', error);
        });
    }

    putTrackerDataUpdate(point) {
        console.log('putTrackerDataUpdate');
        if (this.state.currentTrackerId){
            fetch(`http://${this.state.serverAddress}/trackers/${this.state.currentTrackerId}`, {
                method: "PUT",
                body: JSON.stringify({
                    trackerName: this.state.trackerName,
                    sensorTime: point.timestamp,
                    serverTime: Date.now(),
                    latitude: point.latitude,
                    longitude: point.longitude,
                    altitude: point.altitude,
                    speed: point.speed,
                    heading: point.heading
                }),
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then((jsonStoreResp) => {
                console.log('putTrackerDataUpdate response', jsonStoreResp);
                const formattedJSRPromise = jsonStoreResp.text();
                return formattedJSRPromise;
            }).then((formattedJSONStoreResp) => {
                console.log('trackers put resp: ', formattedJSONStoreResp.length);

            }).catch((error) => {
                console.log('trackers post error: ', error);
            });
        }
    }


    deleteAllServerData() {
        console.log('CONNECTING TO SERVER');
        fetch(`http://${this.state.serverAddress}/points`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        }).then((jsonStoreResp) => {
            const formattedJSRPromise = jsonStoreResp.json();
            return formattedJSRPromise;
        }).then((formattedJSONStoreResp) => {
            console.log('JSON STORE GET RESPONSE: ', formattedJSONStoreResp);

            for (const point of formattedJSONStoreResp) {
                fetch(`http://${this.state.serverAddress}/points/${point.id}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then((r) => {
                    console.log('deleted', point.id)
                }).catch((error) => {
                    console.log('DETETION ERROR: ', error);
                })
            }
        }).catch((error) => {
            console.log('DETETION ERROR: ', error);
        });


    }

    displayedMessage() {
        if (this.state.connected) {
            return 'I SEE YOU!'
        } else {
            return 'I DONT SEE YOU!'
        }
    }

    initGeolocationSensorPolling() {
        console.log('INIT SENSOR POLLING');
        // console.log(navigator);
        // console.log(navigator.geolocation);
        // console.log(navigator.geolocation.getCurrentPosition);

        navigator.geolocation.getCurrentPosition((currentPosition) => {
            console.log('INIT:', currentPosition.coords.latitude + ' , ' + currentPosition.coords.longitude);
            console.log(currentPosition);
        });

        let numberOfRequest = 0;
        if (!this.state.pollInterval) {
            const pollInterval = setInterval(() => {
                console.log(numberOfRequest);
                navigator.geolocation.getCurrentPosition((currentPosition) => {
                    numberOfRequest += 1;
                    console.log('POLL ' + numberOfRequest + ' : ' + currentPosition.coords.latitude + ' , ' + currentPosition.coords.longitude);

                    if (this.state.connected) {
                        const pointToSave = {
                            latitude: currentPosition.coords.latitude,
                            longitude: currentPosition.coords.longitude,
                            altitude: currentPosition.coords.altitude,
                            heading: currentPosition.coords.heading,
                            speed: currentPosition.coords.speed,
                            timestamp: currentPosition.timestamp
                        };

                        this.savePointToServer(pointToSave);
                        this.putTrackerDataUpdate(pointToSave);
                    }

                });
            }, 500);

            this.setState({
                pollInterval
            });
        }

        console.log('INTERVAL SET!');
    }

    stopGeolocationSensorPolling(){
        if (this.state.pollInterval) {
            clearInterval(this.state.pollInterval);
            this.setState({
                pollInterval: null
            })
        }
    }


    render() {
        console.log('RENDER!');
        return (
            <View style={{alignItems: 'center'}}>
                <Text>{this.displayedMessage()}</Text>
                <TextInput
                    placeholder="Enter tracker name"
                    onChangeText={(newTrackerName) => {
                        this.setState({
                            trackerName: newTrackerName
                        });
                        console.log('tracker name changed: ', newTrackerName)
                    }}
                    value={this.state.trackerName}
                />
                <TextInput
                    placeholder="Enter server address"
                    onChangeText={(newAddress) => {
                        this.setState({
                            serverAddress: newAddress
                        });
                        console.log('address changed: ', newAddress)
                    }}
                    value={this.state.serverAddress}
                />
                <Text>
                    *
                </Text>
                <Button
                    disabled={this.state.connected}
                    onPress={() => {
                        console.log('connect!');
                        this.connectToServer();
                    }}
                    title="Connect to server"
                />
                <Text>
                    *
                </Text>
                <Button
                    disabled={!this.state.connected}
                    onPress={() => {
                        console.log('disconnect!');
                        this.disconnectFromServer();
                    }}
                    title="Disconnect from server"
                />
                <Text>
                    *
                </Text>
                <Button
                    onPress={() => {
                        console.log('disconnect!');
                        this.deleteAllServerData();
                    }}
                    title="Delete all server data"
                />
                <Text>
                    *
                </Text>
            </View>
        );
    }
}
